require("modes") 


-- Utility

function is_fluid_source(entity) 
	local p = entity.prototype
	return p.resource_category == "basic-fluid" or (p.mineable_properties and p.mineable_properties.products and p.mineable_properties.products[1] and p.mineable_properties.products[1].type == "fluid")
end

function output(s)
	for _, player in pairs(game.players) do
		player.print("Ore Chaos: " .. s)
	end
end

function default(v, w)
	if v == nil then return w end
	return v
end

function min(a, b)
	if a < b then return a else return b end
end

function in_list(element, list)
	for other in ipairs(list) do
		if element == other then
			return true
		end
	end
	return false
end

function string.starts(String, Start)
  	return string.sub(String, 1, string.len(Start)) == Start
end

-- Core

function get_strategy(name, in_starting_area)
	-- Return applicable strategy. Depends on relevant extracted tile data. Cached.
	-- Returns -1 if no applicable strategy was found.
	local strat_cache = in_starting_area and global.strategy_cache_start_area or global.strategy_cache_all

	local strat = strat_cache[name]
	if strat == nil then

		local modes = nil
		if not in_starting_area then
			modes = global.strategies_outside_starting_area
		else
			modes = global.strategies_all
		end

		local ind = 0
		local found = false -- Have we found some replacement strategy that we can apply?
		while not found do -- Iterate replacement modes
			ind = ind + 1
			replacement_strategy = modes[ind]
			if replacement_strategy == nil then break end

			function is_empty(c)
				return #c == 0 or c == nil or c == {""}
			end

			local match_all = is_empty(replacement_strategy.name_contains) and is_empty(replacement_strategy.name_include_equal)

			-- if both positive filters are not given, we match everything except the excluded.
			-- otherwise we match only those in the name_contains and the name_include_equal filters

			if match_all then
				found = true
				for j, excluded_name in ipairs(replacement_strategy.name_exclude_equal) do
					if name == excluded_name then
						found = false
						break
					end
				end
			else
				-- Name contains positive filter
				for j, part in ipairs(replacement_strategy.name_contains) do
					local name_found = string.find(name, part)
					found = name_found or found
					if name_found then break end
				end

				-- Name not equal negative filter
				for j, excluded_name in ipairs(replacement_strategy.name_exclude_equal) do
					if name == excluded_name then
						found = false
						break
					end
				end

				-- Name equal positive filter
				for j, equal_name in ipairs(replacement_strategy.name_include_equal) do 
					if equal_name == name or (game.active_mods["angelsinfiniteores"] and "infinite" .. equal_name == name) then
						found = true
						break
					end
				end
			end
		end

		-- Update cache after we have tried to find a strategy.
		if found then
			-- Check if the strategy is valid.
			if replacement_strategy.new_entities ~= nil and next(replacement_strategy.new_entities) ~= nil then
				strat_cache[name] = replacement_strategy
				return replacement_strategy
			else
				strat_cache[name] = -1
				return -1
			end
		else
			strat_cache[name] = -1
			return -1
		end
	else
		return strat
	end
end


function change_ores_area(area, surface)
	-- To each area in the given surface, apply the current replacement mode.
	local entities = surface.find_entities_filtered{area=area, type="resource"}
	
	if entities == nil then
		return
	end

	local chunk_mid = {(area.left_top.x + area.right_bottom.x) / 2, (area.left_top.y + area.right_bottom.y) / 2}
	local distance = math.sqrt(chunk_mid[1] * chunk_mid[1] + chunk_mid[2] * chunk_mid[2])
	local in_starting_area = distance < global.starting_area_radius


	local is_not_fluid = {}
	for _, entity in ipairs(entities) do
		if is_not_fluid[entity.name] then
			change_ores_entity(entity, in_starting_area, distance)
		elseif is_not_fluid[entity.name] == nil then
			is_not_fluid[entity.name] = not is_fluid_source(entity)
			if is_not_fluid[entity.name] then change_ores_entity(entity, in_starting_area, distance) end
		end
	end
end


function change_ores_entity(entity, in_starting_area, distance)
	local name = entity.name
	local replacement_strategy = nil

	local replacement_strategy = get_strategy(name, in_starting_area)

	-- Apply the strategy
	if replacement_strategy ~= -1 then
		local rv = math.random()
		local chance = 0
		local r1 = replacement_strategy.inner_radius
		local r2 = replacement_strategy.outer_radius
		local c1 = replacement_strategy.chance
		local c2 = replacement_strategy.outer_chance

		if distance <= r1 then
			chance = c1
		elseif distance >= r2 then
			chance = c2
		else
			chance = (distance * (c1 - c2) + r1 * c2 - r2 * c1) / (r1 - r2)
		end

		if rv <  chance then
			local new_name = nil
			local acc = 0
			rv = math.random() 
			for res, pr in pairs(replacement_strategy.new_entities) do
				acc = acc + pr
				new_name = res
				if rv < acc then 
					break
				end
			end

			if new_name == nil then
				output("Did not find the replacement ore for " .. entity.name .. ". This error should really not happen.")
			end

			if game.active_mods["angelsinfiniteores"] and string.starts(entity.name, "infinite-") then
				if game.entity_prototypes["infinite-" .. new_name] == nil then
					if not global.prototype_infinite_errors[new_name] then 
						output("Warning. Attempted to replace " .. name .. " with infinite-" .. new_name .. " but no prototype was found. Using non-infinite version instead.")
						global.prototype_infinite_errors[new_name] = true
					end
				else
					new_name = "infinite-" .. new_name
				end
			end
			
			local position_old = entity.position
			local amount_old = entity.amount

			local surface = entity.surface
			entity.destroy()
			local new_entity = surface.create_entity{name=new_name, position=position_old, force="neutral", amount=amount_old}
		end
	end
end
	


-- Mode Change Management

function reinitialize_strategies()
	-- Call if the active mode changes. Resets caches.
	global.starting_area_radius = settings.global["orechaos-starting-area-size"].value
	global.strategies_outside_starting_area = {}
	for _, strategy in ipairs(global.strategies_all or {}) do
		if not (strategy.starting_area_only or false) then
			global.strategies_outside_starting_area[#global.strategies_outside_starting_area + 1] = strategy
		end
	end

	global.strategy_cache_all = {}
	global.strategy_cache_start_area = {}
end


function generate_userdefined_mode()
	local sg = settings.global
	local mode = {}
	for i = 1, 4 do
		local strat = {}

		local name_contains = sg["orechaos-udf-name-contains" .. tostring(i)].value
		strat.name_contains = {}
		for s in string.gmatch(name_contains, '([%w%-]+)') do
			table.insert(strat.name_contains, s)
		end

		local name_exclude_equal = sg["orechaos-udf-name-exclude-equal" .. tostring(i)].value
		strat.name_exclude_equal = {}
		for s in string.gmatch(name_exclude_equal, '([%w%-]+)') do	
			table.insert(strat.name_exclude_equal, s)
		end

		local name_include_equal = sg["orechaos-udf-name-include-equal" .. tostring(i)].value
		strat.name_include_equal = {}
		for s in string.gmatch(name_include_equal, '([%w%-]+)') do
			table.insert(strat.name_include_equal, s)
		end

		strat.starting_area_only = sg["orechaos-udf-starting-area-only" .. tostring(i)].value

		strat.chance = sg["orechaos-udf-chance" .. tostring(i)].value
		strat.outer_chance = sg["orechaos-udf-outer-chance" .. tostring(i)].value
		if strat.outer_chance < 0 then strat.outer_chance = strat.chance end
		strat.outer_radius = sg["orechaos-udf-outer-radius" .. tostring(i)].value
		strat.inner_radius = sg["orechaos-udf-inner-radius" .. tostring(i)].value

		local new_entities = sg["orechaos-udf-new-entities" .. tostring(i)].value
		strat.new_entities = {}


		if in_list(new_entities, {"uniform-all", "uniform-vanilla", "uniform-all-no-uranium"}) then 
			strat.new_entities = new_entities
		else
			for entity_name, chance in string.gmatch(new_entities, "([%w%-]+)%s*=%s*([%w%-%.]+)") do
				strat.new_entities[entity_name] = tonumber(chance)
			end
		end

		mode[i] = strat
	end
	return mode
end


function check_mode(mode_data, show_error, show_success)
	local ret = true

	if mode_data == nil then
		for _, player in ipairs(game.players) do 
			player.print("Invalid mode data.")
		end

		if show_error then
			output("Check Mode Result: false")
		end
		return false
	end

	for _, strategy in ipairs(mode_data) do
		if strategy.new_entities ~= nil then
			for entity_name, chance in pairs(strategy.new_entities) do
				if game.entity_prototypes[entity_name] == nil then
					ret = false
					if show_error then
						output("Resource " .. entity_name .. " does not exist, but it is given as a replacement option.")
					end
				end

				if strategy.outer_radius == strategy.inner_radius then
					ret = false
					if show_error then
						output("Outer Radius is equal Inner Radius in strategy.")
					end
				end
			end
		end
	end



	if show_success then
		output("Check Mode Result: " .. tostring(ret))
	end

	return ret
end


function change_mode_by_data(data, show_error, show_success)
	if data == nil then 
		if show_error then 
			output("Mode chance unsuccessful! Data is empty.")
		end
		return false
	end

	postprocess_mode_data(data)
	if check_mode(data, show_error, show_success) then
		if show_success then
			output("Mode changed.")
		end

		global.strategies_all = data
		reinitialize_strategies()

		global.active = true

		return true
	else
		if show_error then
			output("Mode change unsuccessful!") 
		end
		return false
	end
end


function change_mode_by_name(name, show_error, show_success)
	local mode = orechaos.modes[name]
	local data

	if name == nil then 
		if show_error then
			output("Could not change mode, name is nil.") 
		end
	end

	if name ~= "User Defined" then
		data = orechaos.modes[name]
	else
		data = generate_userdefined_mode()
	end

	if data == nil then
		if show_error then
			output("Could not change mode, name ".. name .. " has no associated mode.")
		end
		return false
	end

	if change_mode_by_data(data, false, false) then
		if show_success then
			output("Mode name: " .. name .. ".") 
		end
		return true
	else 
		if show_error then
			output("Could not change mode to " .. name .. ". Do you have all the ores from the mode in the game?") 
		end
		return false 
	end
end

function change_settings()
	script.on_event(defines.events.on_tick, nil)
	local mode_changed = false
	if global.mode_name ~= nil then
		mode_changed = change_mode_by_name(global.mode_name, 1)
		if mode_changed then return	end
	end


	mode_changed = change_mode_by_name(settings.global["orechaos-mode-name"].value, 1, 1)
	if mode_changed then return end

	if settings.global["orechaos-mode-name"].value ~= "Default" then
		output("Falling back to Default mode.")
		mode_changed = change_mode_by_name("Default", 1)
	end

	if mode_changed then return end

	output("Could not load Default mode, deactivating.")
	global.active = false
end



-- Init


function postprocess_mode_data(data)
	-- Default Values and magic strings

	local function make_uniform_distribution(resources)
		local distribution = {}
		local num = #resources
		for _, name in ipairs(resources) do
			distribution[name] = 1/num
		end
		return distribution
	end

	local all_resources = {}
	local no_uranium = {}
	for name, ent in pairs(game.entity_prototypes) do
		if ent.type == "resource" and ent.infinite_resource == false then
			all_resources[#all_resources + 1] = name
			if name ~= "uranium-ore" then
				no_uranium[#no_uranium + 1] = name
			end
		end
	end

	orechaos.uniform_all = make_uniform_distribution(all_resources)
	orechaos.uniform_no_uranium = make_uniform_distribution(no_uranium)
	orechaos.uniform_vanilla = make_uniform_distribution({"stone", "coal", "copper-ore", "iron-ore"})

	function safe(t) 
		if type(t) == "string" then 
			return {t} 
		elseif t == nil then 
			return {} 
		else 
			return t 
		end
	end

	for _, strat in ipairs(data) do
		strat.name_contains = safe(strat.name_contains)
		strat.name_exclude_equal = safe(strat.name_exclude_equal)
		strat.name_include_equal = safe(strat.name_include_equal)

		if strat.DR == 0 then strat.DR = 1000 end
		strat.chance = default(strat.chance, 0)
		strat.outer_chance = default(strat.outer_chance, strat.chance)

		strat.inner_radius = default(strat.inner_radius, 0)
		strat.outer_radius = default(strat.outer_radius, strat.inner_radius + 1)
		if strat.outer_radius == strat.inner_radius then strat.outer_radius = strat.outer_radius + 1 end

		if strat.new_entities == "uniform-all" then 
			strat.new_entities = orechaos.uniform_all
		elseif strat.new_entities == "uniform-all-no-uranium" then 
			strat.new_entities = orechaos.uniform_no_uranium
		elseif strat.new_entities == "uniform-vanilla" then
			strat.new_entities = orechaos.uniform_vanilla
		end

		strat.starting_area_only = default(strat.starting_area_only, false)
	end
end

-- Event
function init()
	if global.initialized then return end
	global.initialized = true

	global.strategies_outside_starting_area = {}
	global.strategies_all = {}
	global.strategy_cache_start_area = {}
	global.strategy_cache_all = {}
	global.starting_area_radius = 0

	global.prototype_infinite_errors = {}

	global.active = true

	global.rso_active = game.active_mods["rso-mod"] ~= nil
	--global.rso_active = RsoMod ~= nil

	if global.rso_active then
		remote.call("RSO", "disableChunkHandler")
		if global.neighbor_chunks == nil then global.neighbor_chunks = {} end
	end

	if late_modes ~= nil then late_modes() end

	change_settings()
end


function on_settings_changed(event)
	if string.find(event.setting, 'orechaos') then
		script.on_event(defines.events.on_tick, change_settings)
	end
end


local function on_chunk_generated(event)
	if not orechaos or not global.initialized then init() end

	if global.rso_active then 
		remote.call("RSO", "generateChunk", event)
		if not global.active then 
			for _, player in ipairs(game.players) do
				player.print("inactive")
			end
			return
		end

		local chunk_x = math.floor(event.area.left_top.x / 32 + 0.1)
		local chunk_y = math.floor(event.area.left_top.y / 32 + 0.1)

		for x = -2, 2 do
			for y = -2, 2 do
				local chunk_position = {x = chunk_x + x, y = chunk_y + y}
				local chunk_id = chunk_position.x * 65536 + chunk_position.y
				if global.neighbor_chunks[chunk_id] == nil then 
					global.neighbor_chunks[chunk_id] = 0 
				else
					global.neighbor_chunks[chunk_id] = global.neighbor_chunks[chunk_id] + 1
				end

				if global.neighbor_chunks[chunk_id] == 24 then
			 		change_ores_area({left_top = {x = chunk_position.x * 32, y = chunk_position.y * 32}, right_bottom = {x = (chunk_position.x + 1) * 32, y = (chunk_position.y + 1) * 32}}, event.surface)
			 		global.neighbor_chunks[chunk_id] = nil
				end
			end
		end
	else
		if not global.active then return end
		change_ores_area(event.area, event.surface)
	end
end


-- Events
script.on_event(defines.events.on_chunk_generated, on_chunk_generated)
script.on_event(defines.events.on_runtime_mod_setting_changed, on_settings_changed)



-- Remote Interface

remote.add_interface("Ore_Chaos", {
	-- /c remote.call("Ore_Chaos", "change_mode", mode_name)
	-- /c remote.call("Ore_Chaos", "change_mode", mode_name, show_error, show_success)
	-- WARNING! This will permanently override the mod-settings on the save until you call the function with mode_name=nil, at which point it adopts mod-setting values again.
	change_mode = function(name, show_error, show_success)
		if name == nil then 
			global.mode_name = nil
			change_settings()
		else
			if change_mode_by_name(name, show_error, show_success) then
				global.mode_name = name
			end
		end
	end,

	-- remote.call("Ore_Chaos", "change_ores_area", {left_top = {x=-4, y=-4}, right_bottom = {x=4, y=4}}, "nauvis")
	-- from console:
	-- /c local radius = 10; remote.call("Ore_Chaos", "change_ores_area", {left_top = {x = game.player.position.x - radius, y = game.player.position.y - radius}, right_bottom = {x = game.player.position.x + radius, y = game.player.position.y + radius}}, game.surfaces.nauvis)  
	change_ores_area = change_ores_area,

	-- We provide an api similar to a part of the rso api we used some time ago.
	on_chunk_generated = on_chunk_generated,
		
	disableChunkHandler = function()
		script.on_event(defines.events.on_chunk_generated, nil)
	end,

	change_ores_entity = change_ores_entity,


	is_active = function()
		return global.active
	end
})

