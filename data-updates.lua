
if settings.startup["orechaos-enable-tech-rebalance"].value then
	-- Circuit Network Changes
	data.raw.technology["circuit-network"].unit.count = 20
	data.raw.technology["circuit-network"].unit.ingredients = {{"science-pack-1", 1}}
	data.raw.technology["circuit-network"].prerequisites = {"automation"}
	data.raw.technology["electronics"].unit.count = 10
	data.raw.technology["logistics"].unit.count = 10
end



function add_remover_recipe(resource_name)
	data:extend
	{
		{
			type = "recipe",
			name = "remove-" .. resource_name,
			category = "remove-crafting",
			enabled = "true",
			energy_required = 5,
			ingredients =
			{
				{resource_name, 20}
			},
			results =
			{
				{type="item", name="void", amount=1, probability=0},
			},
			icon = "__orechaos__/graphics/icons/void.png",
			icon_size = 32,
			order = "remove-" .. resource_name,
			subgroup = "void"
		}
	}
end

if settings.startup["orechaos-enable-remover"].value then
	add_remover_recipe("raw-wood")
	for name, ent in pairs(data.raw.resource) do
		if not ent.infinite and data.raw.item[name] then
			add_remover_recipe(name)
		end
	end
end