Ore Chaos Factorio Mod
=============

Replace a small fraction of the tiles in an ore patch by tiles of a different type. - "Help there's coal in my copper ore!"

Features: 
Ore Replacement in various different flavors,
Tech Rebalance for earlier circuit network and filter inserters
Machine to remove excess resources
Highly configurable

Known Issues
------------

If RSO is enabled, ores are processed only after a suitable number of chunks around them is generated. So they might not be processed on your minimap, but once you get closer to them they the replacements should be shown.

Detailed description
--------------------

Your iron ore patch now contains a small number of stone tiles, coal tiles and copper tiles. Other resource patches are similarly affected and you won't be able to place many mining drills that mine only a single resource. You will need to sort the resources from each other and that is not quite as easy as it sounds.
How did stone get into the copper smelters again? Why is this buffer full already? Why am I getting not nearly as much iron as i should?

The base game does not actually require the use of circuit network all that often; the aim of this mod is to give one more incentive to use the circuit network while staying close to the base game. Of course you can still brute-force your way with filter inserters only, but that makes it harder to achieve good throughput.

There are alternative ore replacement modes supplied, some of them are shown in the image on the mod portal. We provide modes for bobsmods, angel's refining and some alternative vanilla / general modes. You can change the behavior of the mod via mod-settings or via config-file. Make sure you test things in the sandbox scenario first!

The configuration format is quite powerful, so if none of the provided modes is for you, you can make your own through mod settings or the config file. You can make sure that for example only ores outside the starting area are replaced or set different behaviors for different ores. 

The mod includes a machine that can remove excess resources from the game at the price of pollution. This is there in case you find yourself in a situation where it is easier to remove the unwanted resources than to make proper use of it. Or maybe it is there just to taunt you, since why would you ever want to get rid of resources. You can also add a low-tech slow filter inserter through the mod settings, this is mostly for mods like bobs or angels that delay the normal filter inserter, because you do need it in this game.

I also reduce the costs of some early game technologies to make it easier to reach filter inserters and circuit network, this is on by default and can be changed in the settings.


Works well with the following other mods:
Factorissimo2
Bluebuild / Nano Bots



Mod Compatibility
-----------------

The default mode replaces parts of all deposits with iron, copper, stone and coal. There are other modes provided for bobs or angels and you can use all modded ores in a user defined mode.

Known Compatible:
RSO
Bobs Ores
Angels Infinite Ores
Angel's Refining - need to change mod settings
Oreverhaul (this is still very experimental)

Known Incompatible:
--


Credit
------
Thanks to Kryzeth for the config for Angel's Refining. Thanks to everyone who reported bugs!

Changelog.
----------

0.3.9 (5 April 2018)
	Add thorium to bobsmods cfg and fix crash related to fish and wildlife mod

0.3.8 (3 March 2018)
	Hotfix. Fix crash related to mod settings. Mod setting descriptions are slightly incongruous now, but the settings still seem to work.

0.3.7 (20 January 2018)
	Bugfix.

0.3.6 (18 January 2018)
	Fix a leftover bug from transition to 0.16.
	
0.3.5 (15 December 2017)
	Compatible with 0.16

0.3.4 (27 August 17)
	Bugfix for rso compat that was introduced in the last version.
	
0.3.3 (20 August 17)
	Fix crash related to resources without a corresponding item of the same name
	Make compatibility with angel's infinite ores slightly simpler. Remove the related mod setting
	Very slight performance optimization
	Compatibility with geothermal mod.

0.3.2 (26 June 17)
	Multiplayer Fixes
	Fix crash related to user defined settings

0.3.1 (5 June 17)
	Add slow filter inserter
	Fix bug from 0.3.0 where liquids would be replaced too.

0.3.0 (31 May 17)
	Add Mod settings
	Add options to increase replacement chance with distance.
	Parse mode data to ensure data is valid
	Fix RSO compat again

0.2.3 (24 April 17)
	Now 0.15 compatible.

0.2.2 (24 April 17)
	Had default as no starting area in 0.2.1, this was not intended; changed back to apply everywhere. Added Kryzeth's config for Angel's Refining.

0.2.1 (23 April 17)
	RSO compatibility fixes. Added console command for mode changes. Added more modes.

0.2.0 (16 April 17)
	Rewrite to enable better configuration while maintaining efficiency. Now able to change behavior based on starting area and name. Check config.lua!

0.1.1 (13 April 17)
	Add compatibility with Angels Infinite Ores and RSO

0.1.0 (11 April 17)
	Original Release. Includes Ore Mixup, Remover Machine, Technology Rebalance
