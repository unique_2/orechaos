-- modes.lua

-- Ore Chaos modes file
-- =====================
-- If you edit this file, dont forget to make a backup first! (Or you might have to download the mod again).
-- The game will save the current mode for the active save; but this file may be overwritten by updates. So back up your modifications of this somewhere.
-- If you create a configuration that you feel is particularly good, I would appreciate if you share it on the forum or the mod portal page.

if not orechaos then orechaos = {} end
if not orechaos.modes then orechaos.modes = {} end



-- Defaults and Documentation
-- --------------------------
-- Read this before you create your own replacement modes.
-- A replacement mode is an array of replacement strategies. A documented example of a replacement strategy can be seen below. For each ore tile (not deposit, for each tile) we will execute the first replacement strategy that we can. 
-- A replacement strategy contains some filters that will determine wether it will be applied or not. Currently we filter only by name of the ore tile and wether we are in the starting area or not.
-- If the filters return true, apply the strategy: we replace the tile with a chance as given in the strategy. We do not try the next strategy once the filters for a strategy return true, wether we replace the tile or not.

-- Caveats: 
-- We go through all resource type entities, except fluid sources. Make sure to remember this when creating name_contains filters.
-- To emphasize this again, the strategies are tried in order until one is found that is applicable, after this no other strategies will be tried for this tile.


-- Documented Replacement Mode
-- -------------------------------
local replacement_data_example = 
{ -- Array of Replacement Strategies
	{ -- A Replacement Strategy
		name_contains = {"ore", "coal", "stone"}, -- (Array of strings. optional, default = {}) : Apply when one of these is in name of an entity. 
		name_exclude_equal = {}, -- (Array of strings. optional, default = {}) : Exceptions from the name_contains filter.

		name_include_equal = {}, -- (Array of strings. optional, default = {}) : If we don't get a match from the name_contains filter above, we may still apply the strategy if the name is equal to one of these. 

		angels_compatible = true, -- (true / false. optional, default = true) :  Compatibility with angel's infinite ores. If this is true we will replace infinite versions of an ore with an infinite version of the output of the replacement strategy. In that case we also match the infinite version with name_include_equal, e.g. 'name_include_equal = {"iron-ore"}'' will also match "infinite-iron-ore".
		starting_area_only = false, -- (true / false. optional, default = false) : Apply the rule only if the chunk is in the starting area. We model the starting area as a circle with radius as defined in the mod settings.

		chance = 1/6, -- (0 <= x <= 1) : Chance that a replacement is applied.
		outer_chance = nil, -- (0 <= x <= 1. optional, default = chance): Can increase replacement chance proportionally to distance. The chance is the value in strategy.chance below inner_radius, the value of outer_chance above outer_radius and linearly interpolated in between. If the parameter is omitted, we take the same value as in strategy.chance. In principle, this value can be lower than strategy.chance.
		inner_radius = nil, -- (0 < x. optional, default = 0): Radius below which we use the minimal chance value. Only relevant if outer_chance is set.
		outer_radius = nil, -- (0 < x. optional, default = 1): Radius above which we use the outer_chance value as replacement chance. Only relevant if outer_chance is set.

		new_entities = { -- (table of the form {entity_name = chance}, entity_name -- string, 0 <= chance <= 1) : If an entity is replaced, this gives the possible replacements and the chances. Should sum to 1. This must not contain infinite versions if angels_compatible is true.
			stone = 0.25, 
			coal = 0.25, 
			["iron-ore"] = 0.25, 
			["copper-ore"] = 0.25,
		}
	},
}

-- Note that if both name_contains and name_include_equal are {}, then we match every resource. If one of them or both is set, we only match resources that match one (or both) of the filters.

-- Magic Strings:
-- The new_entities field can also be one of the following strings; they will be replaced by some generated data.
-- "uniform-all" is an equal distribution of all noninfinite ore tiles currently in the game.
-- "uniform-all-no-uranium" is an equal distribution of all noninfinite ores except for uranium-ore.
-- "uniform-vanilla" is iron-ore, copper-ore, stone, coal for 1/4 each. Does not include uranium.



-- Default Mode
-- ------------
-- Similar to the first example but makes adjustments for bobsmods.

orechaos.modes["Default"] = {
	{
		chance = 1/6,

		new_entities = {
			stone = 0.25,
			coal = 0.25,
			["iron-ore"] = 0.25,
			["copper-ore"] = 0.25
		}
	}
}




-- Example Mode: Easier Starting Area 
-- -------------------------------------------
-- Replace entities only outside starting area. 
-- to use this, see the end of the file.

orechaos.modes["Default Exclude Starting Area"] = 
{
	{
		-- Catch tiles in starting area so they wont be replaced.
		starting_area_only = true,
		chance = 0,
	},
	{
		name_exclude_equal = {"ruby-ore", "sapphire-ore", "emerald-ore", "amethyst-ore", "topaz-ore", "diamond-ore"},
		starting_area_only = false,
		chance = 1/6,
		new_entities = "uniform-vanilla"
	}
}

-- Example Mode: Increase with distance
-- ------------------------------------
orechaos.modes["Increase with Distance"] =
{
	{
		chance = 0.05,
		outer_chance = 0.5,
		outer_radius = 700,
		new_entities = "uniform-vanilla",
	}
}

-- Example Mode: Increase with distance, Hard
-- ------------------------------------------
orechaos.modes["Increase with Distance Hard"] =
{
	{
		name_exclude_equal = "uranium-ore",
		starting_area_only = true,
		chance = 0.01, 
		inner_radius = 100,
		outer_chance = 0.8,
		outer_radius = 700,
		new_entities = {
			stone = 0.1,
			["copper-ore"] = 0.3,
			["iron-ore"] = 0.4,
			coal = 0.2,
		}
	}, {
		name_include_equal = "uranium-ore",
		starting_area_only = true,
		chance = 0.1,
		outer_chance = 0.5, 
		outer_radius = 500,
		new_entities = {
			stone = 1/6,
			["copper-ore"] = 1/3,
			["iron-ore"] = 1/3,
			coal = 1/3,
		}
	}, {
		chance = 0.01, 
		inner_radius = 100,
		outer_chance = 0.8,
		outer_radius = 700,
		new_entities = {
			stone = 0.1,
			["copper-ore"] = 0.3,
			["iron-ore"] = 0.4,
			coal = 0.12,
			["uranium-ore"] = 0.08
		}
	}
}



-- Example Mode: Uniform Distribution
-- -------------------------------------------
-- Everything in all the deposits. Balance for Uranium.
-- To use, see the end of this file.

orechaos.modes["Uniformly Distributed"] = {
	{
		name_exclude_equal = {"uranium-ore"},
		chance = 1, -- Yep.
		new_entities = "uniform-all-no-uranium",
	},
	{
		name_include_equal = {"uranium-ore"},
		chance = 0.4,
		new_entities = "uniform-all",
	}
}

-- Example Mode: Uniform Distribution
-- -------------------------------------------
-- Everything in all the deposits. Even Uranium, joy.
-- To use, see the end of this file.

orechaos.modes["Uniformly Distributed including Uranium"] = {
	{
		name_contains = {"-ore", "coal", "stone", "quartz"},
		chance = 1,
		new_entities = "uniform-all"
	}
}


-- Example Mode: Bits of Everything
-- -----------------------------------------
-- A small bit of everything in all the deposits
-- To use, see the end of this file.

orechaos.modes["Bits of Everything"] = {
	{
		chance = 1/8,
		new_entities = "uniform-all-no-uranium"
	}
}



-- Example Mode: Stone Surplus
-- ------------------------------------
-- Too much stone
-- To use, see the end of this file.

orechaos.modes["Stone Surplus"] = {
	{
		name_contains = {"-ore", "coal", "quartz"},
		chance = 0.5,
		new_entities = {
			stone = 1
		}
	}
}

-- Example Mode: Black Pest
-- ------------------------------------
-- Too much coal
-- To use, see the end of this file.

orechaos.modes["Black Pest"] = {
	{
		name_contains = {"-ore", "stone", "quartz"},
		chance = 0.5,
		new_entities = {
			coal = 1
		}
	}
}


-- Example Mode: Bobsmods Alternative
-- -------------------------------------------
-- Keep Starting Area clean.
-- Process non-bobsmods ores as usual
-- Divide bobsmods ores in tiers and mix in ores of that tier plus default resources. High-Tier are affected more.
-- Note that you might not need this, the default works well enough. 
-- To use this, see the end of the file.

orechaos.modes["Bobsmods Alternative"] = {
	{
		name_contains = {"ore", "stone", "coal", "quartz"},
		starting_area_only = true,
		chance = 0
	},
	{
		name_include_equal = {"tungsten-ore", "gold-ore", "rutile-ore", "cobalt-ore"}, 
		chance = 1/2,

		new_entities = {
			["tungsten-ore"] = 1/7,
			["gold-ore"] = 1/7,
			["rutile-ore"] = 1/7,
			["stone"] = 1/7,
			["coal"] = 1/7,
			["iron-ore"] = 1/7,
			["copper-ore"] = 1/7
		}
	},
	{
		name_include_equal = {"gem-ore"},
		chance = 1/4,

		new_entities = {
			["gem-ore"] = 1/8,
			["tungsten-ore"] = 1/8,
			["gold-ore"] = 1/8,
			["rutile-ore"] = 1/8,
			["stone"] = 1/8,
			["coal"] = 1/8,
			["iron-ore"] = 1/8,
			["copper-ore"] = 1/8
		}
	},
	{
		name_include_equal = {"cobalt-ore"}, 
		chance = 1/2,

		new_entities = {
			["cobalt-ore"] = 1/8,
			["tungsten-ore"] = 1/8,
			["gold-ore"] = 1/8,
			["rutile-ore"] = 1/8,
			["stone"] = 1/8,
			["coal"] = 1/8,
			["iron-ore"] = 1/8,
			["copper-ore"] = 1/8
		}
	},
	{
		name_include_equal = {"lead-ore", "bauxite-ore", "tin-ore"},
		chance = 1/3,

		new_entities = {
			["stone"] = 1/7,
			["coal"] = 1/7,
			["iron-ore"] = 1/7,
			["copper-ore"] = 1/7,
			["lead-ore"] = 1/7,
			["bauxite-ore"] = 1/7,
			["tin-ore"] = 1/7,
		}
	}, 
	{
		name_include_equal = {"zinc-ore", "silver-ore", "nickel-ore"},
		chance = 1/3,

		new_entities = {
			["tin-ore"] = 1/5,
			["bauxite-ore"] = 1/5,
			["zinc-ore"] = 1/5,
			["silver-ore"] = 1/5,
			["nickel-ore"] = 1/5,
		}
	},
	{
		name_include_equal = {"sulfur"},
		chance = 1/3,

		new_entities = {
			["stone"] = 1/10,
			["coal"] = 1/10,
			["iron-ore"] = 1/10,
			["copper-ore"] = 1/10,
			["zinc-ore"] = 1/10,
			["silver-ore"] = 1/10,
			["nickel-ore"] = 1/10,
			["lead-ore"] = 1/10,
			["bauxite-ore"] = 1/10,
			["sulfur"] = 1/10
		}
	},
	{
		name_include_equal = "thorium-ore",
		change = 1/3,
		new_entities = {
			["stone"] = 1/11,
			["coal"] = 1/11,
			["iron-ore"] = 1/11,
			["copper-ore"] = 1/11,
			["zinc-ore"] = 1/11,
			["silver-ore"] = 1/11,
			["nickel-ore"] = 1/11,
			["lead-ore"] = 1/11,
			["bauxite-ore"] = 1/11,
			["thorium-ore"] = 1/11,
			["gold-ore"] = 1/11
		}
	},
	{
		name_contains = {"-ore", "coal", "stone", "quartz"}, 
		name_exclude_equal = {"ruby-ore", "sapphire-ore", "emerald-ore", "amethyst-ore", "topaz-ore", "diamond-ore"}, 

		chance = 1/6,

		new_entities = {
			stone = 1/4,
			coal = 1/4,
			["iron-ore"] = 1/4, 
			["copper-ore"] = 1/4
		}
	}
}


-- User Submitted Modes
-- ====================

-- Angels Refining Ores (Author: Kryzeth)
-- ---------------------------------------

orechaos.modes["Angels"] = {
    {
        -- Tier 1 (stiratite, saphirite, rubyte, bobmonium)
        name_contains = {"ore1", "ore3", "ore5", "ore6"},
        chance = 1/4,
        new_entities =
        {
            ["angels-ore1"] = 0.25,
            ["angels-ore3"] = 0.25,
            ["angels-ore5"] = 0.25,
            ["angels-ore6"] = 0.25,
        }
    },
    {
        -- Tier 2 (jivolite, crotinnium)
        name_contains = {"ore2", "ore4"},
        chance = 1/3,
        new_entities =
        {
            ["angels-ore1"] = 0.05,
            ["angels-ore3"] = 0.05,
            ["angels-ore5"] = 0.05,
            ["angels-ore6"] = 0.05,
            ["angels-ore2"] = 0.4,
            ["angels-ore4"] = 0.4,
        }
    }
}

orechaos.modes["Angels, Exclude Starting Area"] = {
    {
        -- Catch tiles in starting area so they wont be replaced.
        name_contains = {"angels"},
        starting_area_only = true,
        chance = 0,
    },
    {
        -- Tier 1 (stiratite, saphirite, rubyte, bobmonium)
        name_contains = {"ore1", "ore3", "ore5", "ore6"},
        chance = 1/4,
        new_entities =
        {
            ["angels-ore1"] = 0.25,
            ["angels-ore3"] = 0.25,
            ["angels-ore5"] = 0.25,
            ["angels-ore6"] = 0.25,
        }
    },
    {
        -- Tier 2 (jivolite, crotinnium)
        name_contains = {"ore2", "ore4"},
        chance = 1/3,
        new_entities =
        {
            ["angels-ore1"] = 0.05,
            ["angels-ore3"] = 0.05,
            ["angels-ore5"] = 0.05,
            ["angels-ore6"] = 0.05,
            ["angels-ore2"] = 0.4,
            ["angels-ore4"] = 0.4,
        }
    }
}



-- If you need to reference the game script variable, insert your mode here. These are not available from the mod settings, only from console call  /c remote.call("Ore_Chaos", "change_mode", my-mode").
function late_modes()
	-- Example Modes: One Resource Only
	-- --------------------------------
	-- Replaces all spawned resources by a single ore type.
	-- Named "stone-only", "iron-ore-only", "zinc-ore-only" etc.

	for name, ent in pairs(game.entity_prototypes) do
		if ent.type == "resource" and ent.infinite_resource == false then
			orechaos.modes[name .. "-only"] = {
				{
					name_contains = {"-ore", "stone", "coal", "quartz"},
					chance = 1,
					new_entities = {
						[name] = 1
					}
				}
			}
		end
	end
end



-- All Modes
-- ---------

-- Variable referenced for available modes in mod settings.

orechaos.all_mode_names = {}

for n, _ in pairs(orechaos.modes) do
	table.insert(orechaos.all_mode_names, n)
end
table.insert(orechaos.all_mode_names, "User Defined")


