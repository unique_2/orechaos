require("modes")

data:extend
{
	{
		-- Enable Tech Rebalance
		name = "orechaos-enable-tech-rebalance",
		type = "bool-setting",
		setting_type = "startup",
		default_value = true,
		order = "b",
	},
	{
		-- Enable Remover
		name = "orechaos-enable-remover",
		type = "bool-setting",
		setting_type = "startup",
		default_value = true,
		order = "a",
	},
	{
		-- Enable Slow Filter Inserter
		name = "orechaos-enable-slow-filter-inserter",
		type = "bool-setting",
		setting_type = "startup",
		default_value = false,
		order = "c",
	},
	{
		-- Starting Area Size
		name = "orechaos-starting-area-size",
		type = "double-setting",
		setting_type = "runtime-global",
		default_value = 150.0,
		minimum_value = 0.0,
		order = "0b",
	},
	{
		-- Mode Name
		name = "orechaos-mode-name",
		type = "string-setting",
		setting_type = "runtime-global",
		default_value = "Default",
		allowed_values = orechaos.all_mode_names,
		order = "0a",
	},
	{
		-- Description of Strategies Logic
		name = "orechaos-mode-description-dummy",
		type = "string-setting",
		setting_type = "runtime-global",
		default_value = "Read the Descriptions!",
		allowed_values = {"Read the Descriptions!"},
		order = "0c",
	},
}

for i = 1, 4 do
	local str_i = tostring(i)
	local udf_chance_default = 0
	local udf_entities_default = ""
	if i == 1 then
		--udf_chance_default = 1/6
		udf_entities_default = "stone = 0.25, coal = 0.25, iron-ore = 0.25, copper-ore = 0.25"
	end

	data:extend
	{
		{
			name = "orechaos-udf-starting-area-only" .. str_i,
			type = "bool-setting",
			setting_type = "runtime-global",
			default_value = false,
			order = str_i .. "b",
			localised_name = "User Defined: Strat " .. str_i .. ": Starting Area Only [?]",
			localised_description = "If this is ticked, the rule will only be applied to starting area tiles."
		},
		{
			name = "orechaos-udf-name-contains" .. str_i,
			type = "string-setting",
			setting_type = "runtime-global",
			allow_blank = true,
			default_value = "",
			order = str_i .. "c",
			localised_name = "User Defined: Strat " .. str_i .. ": Name Contains [?]",
			localised_description = "Replace an ore tile if its name contains one of the strings here. Comma-separated list of parts of script names of entities. Script names are usually lower-case with spaces replaced by dashes (Iron Ore -> iron-ore etc.). If both this and Name Include Equal are left blank, we match every non-fluid resource. Otherwise we take the union of the matches from this and Name Include Equal.",
		}, 
		{
			name = "orechaos-udf-name-exclude-equal" .. str_i,
			type = "string-setting",
			setting_type = "runtime-global",
			allow_blank = true,
			default_value = "",
			order = str_i .. "d",
			localised_name = "User Defined: Strat " .. str_i .. ": Name Exclude [?]",
			localised_description = "Do not replace an ore tile if its name is one of these. Comma-separated list of script names of entities, for example 'iron-ore, bobmonium-ore'. Default is empty."
		},
		{
			name = "orechaos-udf-name-include-equal" .. str_i,
			type = "string-setting",
			setting_type = "runtime-global",
			allow_blank = true,
			default_value = "",
			order = str_i .. "e",
			localised_name = "User Defined: Strat " .. str_i .. ": Name Include [?]",
			localised_description = "Do replace an ore tile if its name is equal to one of these. Comma-separated list of script names of entities, for example 'iron-ore, bobmonium-ore'. Default is empty."
		},
		{
			name = "orechaos-udf-chance" .. str_i,
			type = "double-setting",
			setting_type = "runtime-global",
			default_value = udf_chance_default,
			maximum_value = 1,
			minimum_value = 0,
			order = str_i .. "f",
			localised_name = "User Defined: Strat " .. str_i .. ": Replacement Chance (Inner) [?]",
			localised_description = "Chance to replace an individual ore tile - ranges from 0 to 1. Can optionally be made to scale with distance. In that case the chance is this one at a distance below the inner radius, the next value at distances above the outer radius, and linearly interpolated in between."
		},
		{
			name = "orechaos-udf-outer-chance" .. str_i,
			type = "double-setting",
			setting_type = "runtime-global",
			default_value = -1,
			maximum_value = 1,
			minimum_value = -1,
			order = str_i .. "g",
			localised_name = "User Defined: Strat " .. str_i .. ": Replacement Chance Maximum [?]",
			localised_description = "Chance to replace an individual ore tile at Max Radius - ranges from 0 to 1. See Replacement Chance Inner. Can be set negative, in that case we dont scale with distance."
		},
		{
			name = "orechaos-udf-inner-radius" .. str_i,
			type = "double-setting",
			setting_type = "runtime-global",
			default_value = 0,
			maximum_value = 1,
			minimum_value = 0,
			order = str_i .. "h",
			localised_name = "User Defined: Strat " .. str_i .. ": Replacement Chance Inner Radius [?]",
			localised_description = "Radius below which we apply the usual replacement chance."
		},
		{
			name = "orechaos-udf-outer-radius" .. str_i,
			type = "double-setting",
			setting_type = "runtime-global",
			default_value = 0,
			maximum_value = 1,
			minimum_value = 0,
			order = str_i .. "i",
			localised_name = "User Defined: Strat " .. str_i .. ": Replacement Chance Outer Radius [?]",
			localised_description = "Radius above which we apply the outer replacement chance."
		},
		{
			name = "orechaos-udf-new-entities" .. str_i,
			type = "string-setting",
			setting_type = "runtime-global",
			allow_blank = true,
			default_value = udf_entities_default,
			order = str_i .. "k",
			localised_name = "User Defined: Strat " .. str_i .. ": New Entities [?]",
			localised_description = "If the ore tile is replaced, replace it by one of these entities with the corresponding chance. Comma-separated list of the form 'ore-tile1 = chance1, ore-tile2 = chance2', for example 'copper-ore = 0.3, iron-ore = 0.7'. Names must be script names of entities. Chances must be positive and add to (roughly) 1.",
		},
	}
end