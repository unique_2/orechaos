require("util")

local function update_table_recursive(tab, changes)
	if tab == nil then return table.deepcopy(changes) end
	
	for k, v in pairs(changes) do
		if type(v) == "table" then
			tab[k] = update_table_recursive(tab[k], v)
		else
			tab[k] = v
		end
	end
	
	return tab
end

local function copy_prototype(prototype, changes)
	local p = table.deepcopy(prototype)
	update_table_recursive(p, changes)
	return p
end

if settings.startup["orechaos-enable-remover"].value then

	-- Categories
	data:extend(
	{
		{
			type = "recipe-category",
			name = "remove-crafting",
		} 
		,{
			type = "item-subgroup",
			name = "void",
			inventory_order = "d-m",
			order = "zzzz",
			group = "intermediate-products",
		},
	})

	-- Items
	data:extend({
		{
			type = "item",
			name = "void",
			category = "remove-crafting",
			enabled = "true",
			icon = "__orechaos__/graphics/icons/void.png",
			icon_size = 32,
			order = "remover",
			flags = {hidden},
			stack_size = 50,
			subgroup = "void"
		}
	})

	-- Recipes
	data:extend(
	{
		{
			type = "recipe",
			name = "remover",
			category = "crafting",
			energy_required = 3,
			enabled = "true",
			ingredients =
			{
			{"iron-plate", 5},
			{"iron-gear-wheel", 5},
			{"electronic-circuit", 5},
			},
			result = "remover"
		},
	})


	data:extend(
	{
		{
			type = "item",
			name = "remover",
			icon = "__orechaos__/graphics/icons/assembling-machine-purple.png",
			icon_size = 32,
			flags = {"goes-to-quickbar"},
			subgroup = "production-machine",
			order = "e[remover]",
			place_result = "remover",
			stack_size = 50
		}
	})


	data:extend(
	{
		{
			type = "assembling-machine",
			name = "remover",
			icon = "__orechaos__/graphics/icons/assembling-machine-purple.png",
			icon_size = 32,
			flags = {"placeable-neutral", "placeable-player", "player-creation"},
			minable = {hardness = 0.2, mining_time = 0.5, result = "remover"},
			max_health = 250,
			corpse = "big-remnants",
			dying_explosion = "medium-explosion",
			source_inventory_size = 1,
			resistances =
			{
				{
					type = "fire",
					percent = 70
				}
			},
			collision_box = {{-1.2, -1.2}, {1.2, 1.2}},
			selection_box = {{-1.5, -1.5}, {1.5, 1.5}},
			animation =
			{
				filename = "__orechaos__/graphics/entity/assembling-machine-purple/assembling-machine-purple.png",
				priority = "high",
				width = 113,
				height = 99,
				frame_count = 32,
				line_length = 8,
				shift = {0.4, -0.06}
			},
			open_sound = { filename = "__base__/sound/machine-open.ogg", volume = 0.85 },
			close_sound = { filename = "__base__/sound/machine-close.ogg", volume = 0.75 },
			vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
			working_sound =
			{
				sound = {
					{
					filename = "__base__/sound/assembling-machine-t2-1.ogg",
					volume = 0.8
					},
					{
					filename = "__base__/sound/assembling-machine-t2-2.ogg",
					volume = 0.8
					},
				},
				idle_sound = { filename = "__base__/sound/idle1.ogg", volume = 0.6 },
				apparent_volume = 1.5,
			},
			crafting_categories = {"remove-crafting"},
			crafting_speed = 1,
			energy_source =
			{
				type = "electric",
				usage_priority = "secondary-input",
				emissions = 0.4 / 2.5
			},
			energy_usage = "150kW",
			ingredient_count = 1,
			module_specification =
			{
				module_slots = 4
			},
			allowed_effects = {"consumption", "speed", "pollution"}
		}
	})

end -- enable_remover





if settings.startup["orechaos-enable-slow-filter-inserter"].value and data.raw.inserter["slow-filter-inserter"] == nil then
tint1 = {r = 1, g = 0.7, b = 0.7, a = 1}

local slow_filter_inserter_ent = copy_prototype(data.raw.inserter["filter-inserter"], {
	name = "slow-filter-inserter",
	icon = "__base__/graphics/icons/filter-inserter.png",
	icon_size = 32,
	minable = {hardness = 0.2, mining_time = 0.2, result = "slow-filter-inserter"},
	max_health = 100,
	energy_source =
	{
		type = "electric",
		usage_priority = "secondary-input",
		drain = "0.4kW"
	},
	extension_speed = 0.07,
	rotation_speed = 0.01,
	filter_count = 5,
	
	hand_base_picture =
	{
		tint = tint1,
		hr_version = {
			tint = tint1,
		}
	},
	hand_closed_picture =
	{
	tint = tint1,
	hr_version = {
		tint = tint1
		}
	},
	hand_open_picture =
	{
		tint = tint1,
		hr_version = {
			tint = tint1
		}
	},
	platform_picture =
	{
		sheet =
		{
			tint = tint1,
			hr_version = {
				tint = tint1,
			}
		}
	},
})

data:extend{
	slow_filter_inserter_ent,

	{
		type = "item",
		name = "slow-filter-inserter",
		icon = "__orechaos__/graphics/icons/slow-filter-inserter.png",
		icon_size = 32,
		tint = tint1,
		flags = {"goes-to-quickbar"},
		subgroup = "inserter",
		order = "bb[inserter-slow-filter]",
		place_result = "slow-filter-inserter",
		stack_size = 50
	},
	{
		type = "recipe",
		name = "slow-filter-inserter",
		enabled = true,
		ingredients =
		{
			{"iron-plate", 2},
			{"iron-gear-wheel", 1},
			{"copper-cable", 3},
		},
		result = "slow-filter-inserter",
		requester_paste_multiplier = 4,
	},
} 
end -- Enable Slow Filter Inserter